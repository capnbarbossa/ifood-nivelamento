<?php

namespace App\Order\Interfaces;

interface OrderModelInterface {

    public function with($relations);
    
    public function get();

    public function store($data);

    public function fill($data);

    public function save();
}