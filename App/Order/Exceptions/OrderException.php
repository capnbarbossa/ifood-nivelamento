<?php

namespace App\Order\Exceptions;

use Exception;

class OrderException extends Exception {
    protected $message;
    
    public function orderMessage() {
        
        $this->message = 'An error occurred on line: ' . $this->getLine() . ' with the following description: " ' . $this->getMessage() . ' ".';
        
        return $this->message;
    }

    public function orderErrorLog(): void
    {
        $customLogMessage = " Exception: ". date('d-m-Y H:i:s') . ' -> ' . $this->getMessage() . ' ON LINE: ' . $this->getLine() . $this->getTraceAsString() . PHP_EOL;
        
        $f = fopen('public/log/error_log.txt', 'a');
        fwrite($f, $customLogMessage);
        fclose($f);
    }
}