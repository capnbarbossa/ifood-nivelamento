<?php
namespace App\Home\Repository;

use App\Home\Model\HomeModel;

class HomeRepository {
    
    public function __construct(HomeModel $model)
    {
        $this->model = $model;
    }

    public function getUser(){
        return $this->model->select();
    }
}