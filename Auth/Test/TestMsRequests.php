<?php

namespace Auth\Test;

use Exception;
use Src\Request;
use PHPUnit\Framework\TestCase;

class TestMsRequests extends TestCase {

    
    const HOST = '172.21.0.1';
    const PORT = '30500';
    
    public function setUp(): void 
    {
        parent::setUp();
    }

   
    public function testShouldPassWhenHeadersHasValidArray()
    {
       $this->expectNotToPerformAssertions();

       $req = new Request(['Content-Type' => 'application/json','Bearer' => 'dW1hb2JyYWRvYmFydWxobw==']);
       $req->validateHeaders();
       
       //$this->assertTrue($req->validateHeaders() instanceof Request)

    }

    public function testShouldNotPassWhenHeadersDoesntHasBearer()
    {
       $this->expectException(Exception::class);

       (new Request(['Content-Type' => 'application/json']))->validateHeaders();
       
    }

    public function testShouldPassIfHostIsAccessible()
    {
        $ret = fsockopen(self::HOST, self::PORT) ? true : false;
        
        $this->assertTrue($ret);
    }
}