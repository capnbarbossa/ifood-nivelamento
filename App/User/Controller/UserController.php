<?php
namespace App\User\Controller;


use App\Base\BaseController;
use App\User\Model\UserModel;
use App\User\Repository\UserRepository;
use Exception;

class UserController extends BaseController 
{
    private $repository;

    public function __construct(UserModel $model)
    {
        $this->repository = new UserRepository($model);
    }

    public function index()
    {
        try{
            $user = $this->repository->list();

            echo $this->load('users.index', 'data', $user);

        }catch(Exception $e) {
            echo $e->getMessage();
        }
        
    }

}