@extends("layouts.main")
@section('content')
<section class="content-header">
  <h1>
    Página inicial
    <small>Usuários</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
    <li class="active">Usuários</li>
  </ol>

  <ul>
    @foreach($data as $v)
      <li>{{$v->name}}</li>
    @endforeach
  </ul>
</section>

@endsection