<?php
namespace App\Order\Repository;

use App\Base\BaseRepository;
use App\Order\Interfaces\OrderModelInterface;
use App\Order\Model\OrderModel;
use Illuminate\Database\Eloquent\Collection;


class OrderRepository extends BaseRepository {

    private $model;
    private $message;

    public function __construct(OrderModel $model)
    {
        $this->model = $model;    
    }

    public function getById(int $id): Collection
    {
        return $this->model->find($id);
    }
    
    public function store($data): bool
    {
        $this->model->fill($data);
        return $this->model->save();
    }

    public function list()
    {   
        $content = $this->model->with('items')->get();

        if($content->isEmpty()) {
            return $content;
        }        
        
        return $content;
    }

    public function delete($id): bool
    {
        return $this->model->where('id', $id)->delete();
    }

    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    public function getMessage(){
        return $this->message;
    }

    public function process(): bool
    {
        if($this->getMessage() === ''){
            return false;
        }
        
        return true;
    }

}