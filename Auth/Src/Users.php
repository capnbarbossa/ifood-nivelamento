<?php

namespace Src;

class Users { 

    public function __construct()
    {
        echo 'Users Enabled';
    }

    public static function listUsers($id)
    {
        $users = [
            23 => 'Adam',
            34 => 'Edward',
            44 => 'Robert',
            1 => 'Dave',
            12 => 'Chris'
        ];

        return $users[$id] ?? null;
        
    }
}