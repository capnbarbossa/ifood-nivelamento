<?php

namespace App\Base;

use Config\Database;

class BaseModel {
    public $conn;

    public function __construct()
    {
        $database = new Database();

        return $this->conn = $database->db;
    }
}
