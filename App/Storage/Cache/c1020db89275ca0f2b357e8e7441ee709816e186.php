<?php $__env->startSection('content'); ?>
  <section class="content-header">
    <h1 style="margin-bottom:10px;">
      Pedidos
      <small>Gerenciamento de Pedidos do sistema</small>
    </h1>
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Novo Pedido</h3>
      </div>
      
        <div class="box-body">
          
          <div class="form-group">
            <label for="exampleInputItem">Item</label>
            <select class="form-control">
              <option></option>
              <option value="item1">Item 1</option>
              <option value="item2">Item 2</option>
              <option value="item3">Item 3</option>
            </select>
          </div>
          
          <div class="form-group">
            <label for="exampleInputQuantity">Quantidade</label>
            <input type="number" class="form-control" id="exampleInputQuantity" placeholder="Selecione a quantidade" name="quantity" min="0">
          </div>

        </div>
        <!-- /.box-body -->          
        <div class="box-footer">
          <button v-on:click="addline();" class="btn btn-warning" >Enviar</button>
        </div>
      
    </div>
  </section>    
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/App/Home/View/orders.blade.php ENDPATH**/ ?>