<?php

namespace App\Order\Interfaces;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface OrderRepositoryInterfaces {

    public function getById(int $id): Collection;

    public function store(array $data): bool;

    public function list();

    public function delete(int $id): bool;

}