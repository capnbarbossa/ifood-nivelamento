<?php

use App\Home\Controller\HomeController;
use App\Lib\Router;
use PHPUnit\Framework\TestCase;

class RouterTest extends TestCase
{
    private $router;

    public function setUp(): void 
    {
        parent::setUp();
        $this->router = new Router();
        $this->router::$routes = [];
    }

    public function test_ShouldNotPassIfRoutesArrayIsEmpty()
    {
        $this->router::get('/', HomeController::class, 'index', function() {});
        $route = $this->router::$routes;
        $this->assertNotEmpty($route);
    }

    public function test_ShouldNOtPassIfRouteMethodDoesNotExists()
    {
        $moduleController = HomeController::class;
        $this->router::get('/', $moduleController , 'redisTest', function() {});
        $controllerMethod = $this->router::$routes[0]['controllerMethod'];
        
        $this->assertTrue(method_exists($moduleController, $controllerMethod));
    }

    public function test_ShouldNotPassIfPathIsNotSet()
    {
        $moduleController = HomeController::class;
        $this->router::get('/', $moduleController , 'index', function() {});

        $this->assertArrayHasKey('path', $this->router::$routes[0]);
    }

     /**
     * @dataProvider getRequestParamKeysDataProvider
     */
    public function test_ShouldCheckIfAllGetMethodParamsArePresent($key)
    {
        $moduleController = HomeController::class;
        $this->router::get('/', $moduleController , 'index', function() {});

        $payload = $this->router::$routes[0];

        $this->assertArrayHasKey($key, $payload);
    }

    
    public function getRequestParamKeysDataProvider()
    {
        return [
            'shouldNotPassIfPathKeyIsNotPresent' => [
                'key' => 'path'
            ],
            'shouldNotPassIfFunctionKeyIsNotPresent' => [
                'key' => 'function'
            ],
            'shouldNotPassIfFunctionKeyIsNotPresent' => [
                'key' => 'controller'
            ],
            'shouldNotPassIfControllerMethodKeyIsNotPresent' => [
                'key' => 'controllerMethod'
            ],
            'shouldNotPassIfRequestMethodKeyIsNotPresent' => [
                'key' => 'method'
            ]
        ];
    }

    /**
     * @dataProvider requestParamsDataProvider
     */
    public function test_ShouldVerifyRequestParamsIntegrity($param, $expected)
    {
        $moduleController = HomeController::class;
        $this->router::get('/', $moduleController , 'index', function() {});

        $this->assertEquals($this->router::$routes[0][$param], $expected);
    }

    public function requestParamsDataProvider()
    {
        return [
            'testShouldFailIfPathIsEmpty' => [
                'param'    => 'path',
                'expected' => '/'
            ],
            'testShouldPassIfControllerMethodIsNotEmpty' => [
                'param' => 'controllerMethod',
                'expected' => 'index'
            ],
            'testShouldPassIfCallbackFunctionIsPresent' => [
                'param' => 'function',
                'expected' => function(){}
            ],
            'testShouldPassIfControllerIsPresent' => [
                'param' => 'controller',
                'expected' => HomeController::class
            ],
            'testShouldPassIfGetRequestMethodIsPresent' => [
                'param' => 'method',
                'expected' => 'GET'
            ]
        ];
    }

   

}