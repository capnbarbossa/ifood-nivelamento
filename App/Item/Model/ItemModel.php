<?php

namespace App\Item\Model;

use App\Order\Model\OrderModel;
use Illuminate\Database\Eloquent\Model;

class ItemModel extends Model {

    protected $table = 'items';

    protected $fillable = [
        'id',
        'name'
    ];

    public function orders()
    {
        return $this->belongsTo(OrderModel::class, 'id');
    }
}