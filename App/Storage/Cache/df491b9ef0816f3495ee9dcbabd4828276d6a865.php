<?php $__env->startSection('content'); ?>
<section class="content-header">
  <h1>
    Página inicial
    <small>Faça seu pedido no meu PEDIDOS</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
    <li class="active">Pedidos</li>
  </ol>
</section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/App/Home/View/home.blade.php ENDPATH**/ ?>