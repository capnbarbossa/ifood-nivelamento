<?php

// require __DIR__.'/bootstrap.php';

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;
use Src\Auth;
use Src\Bollocks;
use Src\Exceptions\AuthException;
use Src\Users;

require __DIR__.'/vendor/autoload.php';

$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];
$c = new \Slim\Container($configuration);
$app = new App($c);

$app->add(function ($request, $response, $next) {
	
    new Auth($request);
    $response = $next($request, $response);
	return $response;
});


$app->get('/bollocks', function() {
    $content = new Bollocks();

    return json_encode($content->boulder());
});

$app->get('/users/{id}', function(Request $request){
    
    try{
        
        $route = $request->getAttribute('route');
        $id = $route->getArgument('id');
        
        return json_encode(Users::listUsers($id));
        
    }catch(AuthException $e){
        return 'Erro '.$e->getMessage();
    }
});

$app->run();