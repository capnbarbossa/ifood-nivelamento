<?php

namespace Src;

use Exception;

class Request {

    public $headers = [];

    public function __construct(array $headers)
    {
        $this->setHeaders($headers);
    }

    public function setHeaders(array $headers): void
    {
        $this->headers = $headers;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }
    
    
    public function validateHeaders(): self
    {
       
        if(!array_key_exists('Bearer', $this->headers)){
            throw new Exception('O cabeçalho bearer é obrigatório');
        }

        if(!array_key_exists('Content-Type', $this->headers)){
            throw new Exception('O cabeçalho Content Type é obrigatório');
        }

        return $this;
    }


}