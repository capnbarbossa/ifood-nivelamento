<?php

use App\Order\Events\OrderEvent;
use App\Order\Model\OrderModel;
use App\Order\Repository\OrderRepository;

require __DIR__.'/bootstrap.php';
require __DIR__.'/vendor/autoload.php';

use App\Home\Controller\HomeController;
use App\Lib\Router;
use App\Order\Controller\OrderController;
use App\User\Controller\UserController;

// use Slim\Http\Request;
// use Slim\Http\Response;
// use Slim\App;
// use Slim\Container;

######################################################################
// ROTA MANUAL (CLASSE PRÓPRIA COM PHP DI)

Router::get('/', HomeController::class, 'index', function() {});

Router::get('/orders', OrderController::class, 'list', function() {});

Router::get('/orders/store', OrderController::class, 'store', function() {});

Router::get('/users', UserController::class, 'index', function() {});

Router::post('/orders/save/', OrderController::class, 'save', function() {});



Router::run();

#########################################################################

// CURL CALL FROM MS TO API WITH BEARER AUTH

// $url = '172.21.0.1:30500/users/44';
// $token = 'dW1hb2JyYWRvYmFydWxobw==';

// header('Content-Type: application/json'); // Specify the data type 

// $ch = curl_init($url); 
// curl_setopt($ch, CURLOPT_URL, $url);
// curl_setopt($ch, CURLOPT_HTTPHEADER, array('Bearer: '.$token));
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// $resp = curl_exec($ch);
// $err = curl_error($ch);

// curl_close($ch);

// echo $resp;
#########################################################################





// PHP EVENTS

// $message = 'Vraudigrau';
// $e = new OrderEvent();

// $e::listen('list', function() {
//     echo 'Evento de listagem disparado';
// });

// $model = new OrderModel();
// $repo = new OrderRepository($model);

// if($repo->list()) {
//     $e::trigger('list');
// };

###################################################################


//SLIM FRAMEWORK
// $configuration = [
//      'settings' => [
//          'displayErrorDetails' => true,
//      ],
//  ];
//  $c = new Container($configuration);
//  $app = new App($c);

//  $app->get('/', function (Request $request, Response $response) {


//      var_dump($request->getHeaders());
//  });

 //$app->run();

 ################################################################