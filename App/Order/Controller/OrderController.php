<?php 

namespace App\Order\Controller;

use App\Base\BaseController;
use App\Item\Repository\ItemRepository;
use App\Order\Repository\OrderRepository;
use Exception;

class OrderController extends BaseController { 

    private $repository;
    private $itemRepository;
    
    public function __construct(OrderRepository $repository, ItemRepository $itemRepository)
    {
        $this->repository = $repository;
        $this->itemRepository = $itemRepository;
    }

    public function index()
    {
        echo $this->load('path:orders.index');
    }

    public function save($data)
    {
      try {
        
        
        $this->repository->store($data);
        
        header('Location: /orders');

      }catch(Exception $e) {
          echo $e->getMessage();
      }
    }

    public function store()
    {
      try {

        
        $items = $this->itemRepository->list();
        
        echo $this->load('orders.store', 'data', $items);

      }catch(Exception $e) {
        
        echo $e->getMessage();
      }
      
    }

    public function list()
    {
      try {
        $list = $this->repository->list();
        echo $this->load('orders.index', 'data', $list);
      }catch(Exception $e) {
        echo $e->getMessage();
      }
    }

   
    
}

