<?php

/**
 * Author: Robert Barbosa
 * 
 * Router class with PHP-DI
 * 
 * 01/2022
 */

namespace App\Lib;

use App\Lib\Interfaces\RouterInterface as InterfacesRouterInterface;
use App\Order\Exceptions\OrderException;
use DI\ContainerBuilder;
use stdClass;
class Router implements InterfacesRouterInterface
{

    public static $routes = [];

    /**
     * Method designed to run the routes triggered by the HTTP methods
     * 
     * @return void
     */
    public static function run(): void
    {
        try {

            $url = (isset($_SERVER['REQUEST_URI']) ? parse_url($_SERVER['REQUEST_URI']) : '/' );
            $method = (isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET');
            $path = (isset($url['path']) ? $url['path'] : '/');
            
            if(!empty(self::$routes)):
                foreach(self::$routes as $route):
                 
                    //REGULAR EXPRESSION MOUNTED
                    $route['path'] = '^'.$route['path'];
                    $route['path'] = $route['path'].'$';
                    
                    if(preg_match("#".$route['path']."#", $path, $matches)){

                        if($method != $route['method']) {
                            throw new OrderException('Método não encontrado');
                        }

                        if(strtolower($method) == strtolower($route['method'])){
                            
                            $build = self::getControllerMethod($route['controller']);
                            $controllerMethod = $route['controllerMethod'];
                            
                            if($method == 'POST') {

                               if($build->$controllerMethod($_POST) != null) {
                                    call_user_func_array($build->$controllerMethod($_POST), $matches);
                                }

                            }else{

                                if($build->$controllerMethod() != null) {
                                    call_user_func_array($build->$controllerMethod(), $matches);
                                }
                            }
                        }
                    }

                endforeach;
            endif;

        }catch(OrderException $e) {
            $e->orderErrorLog();
        }
    }
    
    /**
     * Basic get method 
     * 
     * @return void
     */
    public static function get($path, $controller, $controllerMethod, $function): void
    {
        
        $requestParams = [
            'path' => $path,
            'function' => $function,
            'controller' => $controller,
            'controllerMethod' => $controllerMethod,
            'method'   => 'GET'
        ];

        array_push(self::$routes, $requestParams);
    }

    /**
     * Basic post method 
     * 
     * @return void
     */
    public static function post($path, $controller, $controllerMethod, $function): void
    {
        $payload = filter_var($_POST, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
      
        array_push(self::$routes, [
            'path' => $path,
            'function' => $function,
            'controller' => $controller,
            'controllerMethod' => $controllerMethod,
            'payload'  => $_POST,
            'method'   => 'POST',
        ]);
    }

    public static function getControllerMethod($controller): object
    {
        $builder = new ContainerBuilder();
        return $builder->build()->get($controller);
    }
}

