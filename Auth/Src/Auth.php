<?php

namespace Src;

use Slim\Http\Request;
use Src\Exceptions\AuthException;

class Auth {

    private $request;

    public function __construct(Request $request)
    {
       $this->request = $request;
       return $this->authRequest();
    }

  
    public function authRequest(): bool
    {
        $bearerKey = $this->request->getHeaders();

        if(!isset($bearerKey['HTTP_BEARER'][0]) || empty($bearerKey['HTTP_BEARER'][0])) {
            throw new AuthException('Nenhuma chave encontrada');
        }
        
        return $this->validateKey($bearerKey['HTTP_BEARER'][0]);
    }

    public function validateKey(string $key): bool
    {
        $nativeKey = $this->retrieveKeyFromDB();

        if($nativeKey !== $key) {
            throw new AuthException('A chave informada não é válida ou expirou');
        }

        return $key;
    }

    public function retrieveKeyFromDB(): string
    {
        return base64_encode('umaobradobarulho');
    }

  
}