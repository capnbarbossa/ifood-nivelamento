@extends("layouts.main")
@section('content')
<section class="content-header">
  <h1>
    Página inicial
    <small>Pedidos</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
    <li class="active">Usuários</li>
  </ol>

  <ul>
    @foreach($data as $k => $v)
      <li>{{$v->items->name}} | {{$v->quantity}}</li>
    @endforeach
  </ul>
</section>

@endsection