<?php

namespace App\Order\Events;

class OrderEvent {

    public static $events = [];

    /**
     * @param string $name
     * 
     * @param callable $callback
     * 
     * @return void
     * 
     * Method designed to build array events for further reading
     */
    public static function listen(string $name, callable $callback): void
    {
        self::$events[$name][] = self::checkIfParamIsCallable($callback);
    }

    /**
     * @param string $name
     * 
     * @param $argument
     * 
     * Iterates over the events array calling the designed callback functions of each key
     */
    public static function trigger(string $name, $argument = null)
    {
        if(self::checkIfEventNameIsValid($name)) {
            foreach(self::$events[$name] as $event)
            {
                
                if(is_array($event)) {
                    return call_user_func_array($event, $argument);
                }elseif(!empty($argument)){
                    return call_user_func($event, $argument);
                }else {
                    return call_user_func($event);
                }
            }
        }
    }

    /**
     * @param string $name
     * 
     * @return bool
     * 
     * Verifies if the passed name is a valid string to be used
     */
    public static function checkIfEventNameIsValid(string $name): bool
    {
        return isset($name) && !empty($name) ? true : false;
    }

    /**
     * @param callable $callback
     * 
     * @return callable;
     * 
     * Checks if passed param is a valid callback function
     */
    public static function checkIfParamIsCallable(callable $callback): callable
    {
        return is_callable($callback) ? $callback : null;
    }

}