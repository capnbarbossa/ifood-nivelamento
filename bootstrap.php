<?php

use Illuminate\Database\Capsule\Manager as Capsule;
require_once "vendor/autoload.php";

$capsule = new Capsule;
$capsule->addConnection([
    'driver'   => 'mysql',
    'host'     => '172.21.0.1',
    'database' => 'ifooddb',
    'username' => 'root',
    'password' => '',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'   => '',
    ]);

$capsule->setAsGlobal();
$capsule->bootEloquent();

