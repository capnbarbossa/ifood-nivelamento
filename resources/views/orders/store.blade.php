@extends('layouts.main')
@section('content')
  <section class="content-header">
    <h1 style="margin-bottom:10px;">
      Pedidos
      <small>Gerenciamento de Pedidos do sistema</small>
    </h1>
    <div class="box box-success">
      <form method="POST" action="/orders/save/">
          <div class="box-header with-border">
            <h3 class="box-title">Novo Pedido</h3>
          </div>
          
            <div class="box-body">
              
              <div class="form-group">
                <label for="exampleInputItem">Item</label>
                <select name="item" class="form-control">
                  <option></option>
                   @if(!empty($data))
                      @foreach($data as $item)
                        <option value="{{$item['id']}}">{{$item['name']}}</option>
                      @endforeach
                   @endif
                </select>
              </div>
              
              <div class="form-group">
                <label for="exampleInputQuantity">Quantidade</label>
                <input type="number" class="form-control" id="exampleInputQuantity" placeholder="Selecione a quantidade" name="quantity" min="0">
              </div>

            </div>
            <!-- /.box-body -->          
            <div class="box-footer">
              <button type="submit" class="btn btn-warning" >Enviar</button>
            </div>
       </form>
    </div>
  </section>    
@endsection
