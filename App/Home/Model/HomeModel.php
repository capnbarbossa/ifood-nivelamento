<?php

namespace App\Home\Model;

use App\Base\BaseModel;
use Exception;
use Illuminate\Database\Eloquent\Model;

class HomeModel extends Model {

    protected $table = 'users';
    public $name;
    
    public function select()
    {
        try{

            $users = HomeModel::where('id', 2)->first();
            return $users;

        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

}