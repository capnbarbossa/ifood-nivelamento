<?php

namespace Src\Exceptions;

use Exception;

class AuthException extends Exception {

    protected $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    public function detailedError()
    {
        return 'Ocorreu o seguinte erro '.$this->getMessage().' no arquivo '.$this->getFile().' -> '.$this->getLine().' ';
    }
}