<?php

namespace App\Order\Model;

use App\Item\Model\ItemModel;
use App\Order\Interfaces\OrderModelInterface;
use Illuminate\Database\Eloquent\Model;

class OrderModel extends Model {

    protected $table = 'orders';

    protected $fillable = [
        'id',
        'item',
        'quantity'
    ];

    public function items()
    {
        return $this->hasOne(ItemModel::class, 'id', 'item');
    }
}