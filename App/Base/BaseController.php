<?php

namespace App\Base;

use DI\ContainerBuilder;
use Exception;
use Jenssegers\Blade\Blade;
class BaseController {
    
    public $view;
    
    public function load(string $path, string $prefix = '',$data = []) {

        try{
           
            $this->view = new Blade('resources/views/', 'App/Storage/Cache');
            return $this->view->make($path, [$prefix => $data])->render();

        }catch(Exception $e) {
            echo $e->getMessage();
        }
    }
}
