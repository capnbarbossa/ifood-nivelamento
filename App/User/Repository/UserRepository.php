<?php

namespace App\User\Repository;

use App\Base\BaseRepository;
use App\User\Model\UserModel;

class UserRepository extends BaseRepository {

    public $model;
    public $name;
    public $clearance = [];

    public function __construct(UserModel $model)
    {
        $this->model = $model;
    }

    public function list()
    {
        return $this->model->get();
    }

    public function setFirstName(string $name)
    {
        $this->name = $name;
    }

    public function getFirstName()
    {
        return $this->name;
    }

    public function setClearance(array $clearance)
    {
        $this->clearance = $clearance;
    }

    public function getClearance()
    {
        return $this->clearance;
    }
}