<?php

namespace App\Item\Repository;

use App\Base\BaseRepository;
use App\Item\Model\ItemModel;
use Exception;

class ItemRepository extends BaseRepository {

    public function __construct(ItemModel $model)
    {
        $this->model = $model;
    }


    public function list()
    {
        try {
            
            return $this->model->get();

        }catch(Exception $e) {
            echo $e->getMessage();
        }
        
    }

}