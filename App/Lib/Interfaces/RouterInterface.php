<?php

namespace App\Lib\Interfaces;
use DI\ContainerBuilder;
interface RouterInterface
{
    public static function run(): void;
    public static function get($path, $controller, $controllerMethod, $function): void;
    public static function post($path, $controller, $controllerMethod, $function): void;
    public static function getControllerMethod($controller):object;
}