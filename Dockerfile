FROM php:8.0.8-apache

ARG user
ARG uid

RUN docker-php-ext-install pdo_mysql
RUN apt-get update && apt-get install -y git
RUN apt-get install nano -y

COPY docker-setup/000-default.conf /etc/apache2/sites-available/000-default.conf
RUN a2enmod ssl && a2enmod rewrite

RUN curl -sS https://getcomposer.org/installer -o composer-setup.php
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer


# Set working directory
WORKDIR /var/www




