<?php
namespace App\Base;

class BaseView {

    private $file;
    private $data;

    public function __construct(string $file)
    {
        $this->file = $file;
    }

    public function getData()
    {
        return $this->getData;
    }

    public function load($data)
    {
        $this->data = $data;
        $this->layout = null;

        ob_start();
        
        include ('../App/Home/View/'.$this->file);

        if (null === $this->layout){
            ob_end_flush();
        }else{
            ob_end_clean();

            // Include the layout
            $this->includeFile($this->layout);
        }
    }

    protected function includeFile($file)
    {
        $v = new BaseView($file);
        $v->load($this->data);
        $this->data = $v->getData();
    }
}