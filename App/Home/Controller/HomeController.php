<?php
namespace App\Home\Controller;

use App\Base\BaseController;
use App\Home\Model\HomeModel;
use App\Home\Repository\HomeRepository;
use Exception;


class HomeController extends BaseController {
    
    /**
     * Sets the Home Repository
     *
     * @var HomeRepository
     */

    
    public $repository;

    public function __construct(HomeModel $model){
        
        $this->repository = new HomeRepository($model);
    }


    public function index()
    {
        echo $this->load('home.index');
    }

    public function redisTest() {
        try {
            $redis = new \Predis\Client([
                'host' => '172.22.0.2'
            ]);

            $redis->set('Cop', 'Those bastards lied to me');

            echo $redis->get('design_pattern');
        
            
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
   
}